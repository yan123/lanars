module.exports.setup = function(app, controllers) {
    var express     = require('express');
    var path        = require('path');
    var bodyParser  = require('body-parser');
    var fileUpload  = require('express-fileupload');

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    //Static paths
    app.use('/img',         express.static(path.join(__dirname, '../build/img')));
    app.use('/test',        express.static(path.join(__dirname, '../build/test')));
    app.use('/js',          express.static(path.join(__dirname, '../build/js')));
    app.use('/css',         express.static(path.join(__dirname, '../build/css')));
    app.use('/fonts',       express.static(path.join(__dirname, '../build/fonts')));
    app.use('/templates',   express.static(path.join(__dirname, '../build/templates')));
    app.use('/locals',      express.static(path.join(__dirname, '../build/locals')));
    app.use('/.well-known', express.static(path.join(__dirname, '../cerf/.well-known')));
    //Api

      //login
      app.post    ('/api/login',    controllers.auth.authorization);
      app.get     ('/api/login',    controllers.auth.other);
      app.put     ('/api/login',    controllers.auth.other);
      app.delete  ('/api/login',    controllers.auth.other);

      //current
      app.get     ('/api/me',       controllers.current.getCurrentUser);
      app.put     ('/api/me/:id',   controllers.current.updateUser);

      //user
      app.get     ('/api/user',     controllers.users.SearchUsers);
      app.get     ('/api/user/:id', controllers.users.getUserById);

      //register
      app.post    ('/api/register', controllers.register.createUser);

      //items
      app.get     ('/api/item',     controllers.items.getMyItemsOrSearch);
      app.put     ('/api/item/:id', controllers.items.UpdateMyItems);
      app.put     ('/api/item',     controllers.items.CreateMyItems);
      app.get     ('/api/item/:id', controllers.items.getSingleItem);
      app.delete  ('/api/item/:id', controllers.items.deleteMyItem);
         // image item
         app.post  ('/api/item/:id/image', controllers.image.uploadImage);
         app.delete('/api/item/:id/image', controllers.image.deleteImage);
    //MainPage redirect
      app.all('/*',controllers.admin);
};
