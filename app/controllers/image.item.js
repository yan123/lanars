var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));
var fs = require('fs');
var multer = require("multer");
var express = require("express");
var app = express();

function uploadImage(req, res) {
  var updateId = req.params.id;
  var updateValues = {};
  updateValues.updated_at = Date.now();
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var Item = db.model("Items", dbSchemas.ItemsSchema);
      var User = db.model("Users", dbSchemas.UserSchema);
      User.findOne({ api_token: req.headers.authorization }, function(err, user) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (user){
            Item.findOne({ _id: updateId }, function(err, item){
                if (err) {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: err.message
                    });
                }
                if (item) {
                  if( user["_id"] == item["user_id"] ){
                    uploadFileOther(req, res, "file[0]")
                     .then(function(response) {
                       updateValues.image = '/img/'+response;
                       Item.update({ _id: updateId }, updateValues, {upsert:true}, function(err, doc){
                         if (err) {
                             dbController.closeDBConnection(db, res, 500, {
                                 success: false,
                                 response: false,
                                 message: 'Update error. DB message: '+err.message
                             });
                         }
                         if (doc) {
                             item.user = user;
                             item.image  = updateValues['image'];
                             item.description = updateValues['description'];
                             dbController.closeDBConnection(db, res, 200, {
                                 success: true,
                                 response: item,
                                 message: false
                             });
                         } else {
                             dbController.closeDBConnection(db, res, 500, {
                                 success: false,
                                 response: false,
                                 message: 'Update error'
                             });
                         }
                       })
                    })
                    .catch(function(err) {
                      dbController.closeDBConnection(db, res, 500, {
                          success: false,
                          response: false,
                          message: err
                      });
                    });
                  } else {
                    dbController.closeDBConnection(db, res, 403, {
                        success: false,
                        response: false,
                        message: 'This work does not belong to this user'
                    });
                  }
                } else {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: 'error get Item'
                    });
                }
            });
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
      });
  });
}
function uploadFileOther(req, res, fieldname) {
    return new Promise(function(resole, reject) {
        var nameImg = fieldname+'-'+Date.now() + '.png'
        var storage = multer.diskStorage({
            destination: function(req, file, callback) {
                callback(null, path.join(__dirname, '../../build/img/'));
            },
            filename: function(req, file, callback) {
                callback(null, nameImg);
            }
        });
        var uploadFile = multer({ storage: storage }).array(fieldname, 1);
        uploadFile(req, res, function(err) {
            if (err) {
                console.log(err);
                reject("Error uploading file.");
            } else
            resole(nameImg);
        });
    });
}

function deleteImage(req, res){
  var deleteIdImg = req.params.id;
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var Item = db.model("Items", dbSchemas.ItemsSchema);
      var User = db.model("Users", dbSchemas.UserSchema);
      User.findOne({ api_token: req.headers.authorization }, function(err, user) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (user){
            Item.findOne({ _id: deleteIdImg }, function(err, item){
                if (err) {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: err.message
                    });
                }
                if (item) {
                  if( user["_id"] == item["user_id"] ){
                      Item.update({ _id: deleteIdImg }, {image: "/img/test.jpg"}, {upsert:true}, function(err, doc){
                        if (err) {
                            dbController.closeDBConnection(db, res, 500, {
                                success: false,
                                response: false,
                                message: 'Update error. DB message: '+err.message
                            });
                        }
                        if (doc) {
                            fs.unlinkSync(path.join(__dirname, '../../build')+item.image);
                            dbController.closeDBConnection(db, res, 200, {
                                success: true,
                                response: false
                            });
                        } else {
                            dbController.closeDBConnection(db, res, 500, {
                                success: false,
                                response: false,
                                message: 'Update error'
                            });
                        }
                      })

                  } else {
                    dbController.closeDBConnection(db, res, 403, {
                        success: false,
                        response: false,
                        message: 'This work does not belong to this user'
                    });
                  }
                } else {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: 'error get Item'
                    });
                }
            });
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
      });
  });
}
module.exports.deleteImage   = deleteImage;
module.exports.uploadImage   = uploadImage;
