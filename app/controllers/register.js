var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));

function createUser(req, res){
    var requestBody = req.body;
    dbController.createDBConnection(false, res, function(db, res){

        if (!requestBody.email || !requestBody.password) {

            dbController.closeDBConnection(db, res, 403, {
                success: false,
                response: false,
                message: 'emil and password must be required'
            });

        } else {
          var User = db.model("Users", dbSchemas.UserSchema);
          var newUser = new User({
              email:      requestBody.email,
              name:       requestBody.name || null,
              phone:      requestBody.phone || null,
              api_token:  crypto.randomBytes(32).toString('hex'),
              password:   crypto.createHash('md5').update(requestBody.password).digest('hex')
          });

          newUser.save(function(err, newUser){
              if(err){
                dbController.closeDBConnection(db, res, 500, {
                    success: false,
                    response: false,
                    message: 'This email already exists [' + err.message + ']'
                });
              }

              if(newUser){

                dbController.closeDBConnection(db, res, 200, {
                    success: true,
                    response: {
                      "api_token":           newUser['api_token']
                    },
                    message: false
                });

              } else {

                dbController.closeDBConnection(db, res, 500, {
                    success: false,
                    response: false,
                    message: 'Error in saving User'
                });

              }

          })

        }
    })
}


module.exports.createUser  = createUser;
