var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));



function getMyItemsOrSearch(req, res){
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var User = db.model("Users", dbSchemas.UserSchema);
      var Item = db.model("Items", dbSchemas.ItemsSchema);
      User.findOne({ api_token: req.headers.authorization }, function(err, users) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (users){
              if(!Object.keys(req.query).length) {
                Item.find({user_id: users["_id"] },function(err, items){
                  if (err) {
                    dbController.closeDBConnection(db, res, 500, {
                        success:    false,
                        response:   false,
                        message:    err.message
                    });
                  }
                  if(items){
                      for(var item in items){
                        items[item].user = users;
                      }
                      dbController.closeDBConnection(db, res, 200, {
                          success: true,
                          response: items,
                          message: false
                      });
                  } else {
                    dbController.closeDBConnection(db, res, 500, {
                        success:    false,
                        response:   false,
                        message:    'Error in getting Items list'
                    });
                  }
                })
              } else if(!!Object.keys(req.query).length){
                var requestBody = req.query;
                var updateValuesMask = [
                    'title',
                    'user_id'
                ];
                var updateValues = {};
                for (var key in requestBody) {
                    if (updateValuesMask.indexOf(key) != -1) {
                        updateValues[key] = requestBody[key];
                    }
                }

                Item.find(updateValues, function(err, items){
                    function compareObjects (a, b) {
                      var order_typePlus  =  !req.query.order_type ? -1 : req.query.order_type == 'desc' ? -1 : 1;
                      var order_typeMines =  !req.query.order_type ? 1 : req.query.order_type == 'desc' ? 1 : -1;
                      if((!req.query.order_by) || req.query.order_by == 'created_at'){
                        if (a.created_at > b.created_at) return order_typePlus;
                        if (a.created_at < b.created_at) return order_typeMines;
                      } else {
                        if (a.title > b.title) return order_typePlus;
                        if (a.title < b.title) return order_typeMines;
                      }
                      return 0;
                    };
                    var user_id  = [];
                    for(var key in items){
                        user_id.push(items[key]["user_id"])
                    }
                      User.find( {'_id': user_id}, function(err, users){
                        if (err) {
                            dbController.closeDBConnection(db, res, 500, {
                                success:    false,
                                response:   false,
                                message:    'This login already exists [' + err.message + ']'
                            });
                        }
                        if(users){
                            for(var key in items){
                              for(var keyUsers in users){
                                if(items[key]['user_id'] == users[keyUsers]['_id']) items[key].user = users[keyUsers];
                              }
                            }
                            items.sort(compareObjects);
                            dbController.closeDBConnection(db, res, 200, {
                                success: true,
                                response: items,
                                message: false
                            });

                        } else {
                            dbController.closeDBConnection(db, res, 500, {
                                success:    false,
                                response:   false,
                                message:    'Error in getting Users list'
                            });
                        }
                      })

                })
                // User.find({ "_id" : [ '591564e893282e81a4312883', '5a5b849bf3ad0d5e129ec67a']}, function(err, users){
                //   console.log(users)
                // })
              } else {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'Error params'
                });
              }
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
      });
  });
}


function CreateMyItems(req, res){
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
    var requestBody = req.body;
    if (!requestBody.title || !requestBody.description) {
        dbController.closeDBConnection(db, res, 403, {
            success: false,
            response: false,
            message: 'title and description must be required'
        });
    } else {
        var Item = db.model("Items", dbSchemas.ItemsSchema);
        var User = db.model("Users", dbSchemas.UserSchema);
        User.findOne({ api_token: req.headers.authorization }, { "_id" : 1 }, function(err, users) {
            if (err) {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'This login already exists [' + err.message + ']'
                });
            }
            if (users){
              var newItem = new Item({
                  title:        requestBody.title,
                  description:  requestBody.description,
                  image:        "/img/test.jpg",
                  user_id:      users["_id"],
                  user: {}
              });
              newItem.save(function (err, newItem) {
                  if (err) {
                      dbController.closeDBConnection(db, res, 500, {
                          success: false,
                          response: false,
                          message: err.message
                      });
                  }
                  if (newItem) {
                      dbController.closeDBConnection(db, res, 200, {
                          success: true,
                          response: {
                              "id":           newItem['_id'],
                              "title":        newItem['title'],
                              "updated_at":   newItem['updated_at'],
                              "image":        newItem['image'],
                              "created_at":   newItem['created_at'],
                              "description":  newItem['description'],
                              "user_id":      newItem['user_id'],
                              "user":         users
                          },
                          message: false
                      });
                  } else {
                      dbController.closeDBConnection(db, res, 500, {
                          success: false,
                          response: false,
                          message: 'Error in saving User'
                      });
                  }
              });
            } else {
                dbController.closeDBConnection(db, res, 500, {
                    success:    false,
                    response:   false,
                    message:    'Error in getting Users list'
                });
            }
        });
    }
  });
}

function getSingleItem(req, res){
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var ItemId = req.params.id;
      var User = db.model("Users", dbSchemas.UserSchema);
      var Item = db.model("Items", dbSchemas.ItemsSchema);
      User.findOne({ api_token: req.headers.authorization }, function(err, user) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (user){
              Item.findOne({ "_id": ItemId },function(err, item){
                if (err) {
                  dbController.closeDBConnection(db, res, 500, {
                      success:    false,
                      response:   false,
                      message:    err.message
                  });
                }
                if(item){
                    item.user = user;
                    dbController.closeDBConnection(db, res, 200, {
                        success: true,
                        response: item,
                        message: false
                    });
                } else {
                  dbController.closeDBConnection(db, res, 500, {
                      success:    false,
                      response:   false,
                      message:    'Error in getting item list'
                  });
                }
              })
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
        });
    });
}

function deleteMyItem(req, res) {
    var deletedId = req.params.id;

    dbController.createDBConnection(req.headers.authorization, res, function(db, res){
        var Item = db.model("Items", dbSchemas.ItemsSchema);
        Item.remove({ _id: deletedId }, function(err) {
            if (err) {
                dbController.closeDBConnection(db, res, 500, {
                    success: false,
                    response: false,
                    message: 'Invalid item ID. DB message: '+err
                });
            } else {
                dbController.closeDBConnection(db, res, 200, {
                    success: true,
                    response: '',
                    message: false
                });
            }
        });
    });
}

function UpdateMyItems(req, res){
  var updateId = req.params.id;
  var requestBody = req.body;
  var updateValuesMask = [
      'title',
      'description'
  ];
  var updateValues = {};
  for (var key in requestBody) {
      if (updateValuesMask.indexOf(key) != -1) {
          updateValues[key] = requestBody[key];
      }
  }
  updateValues.updated_at = Date.now();
  dbController.createDBConnection(req.headers.authorization, res, function(db, res){
      var Item = db.model("Items", dbSchemas.ItemsSchema);
      var User = db.model("Users", dbSchemas.UserSchema);
      User.findOne({ api_token: req.headers.authorization }, function(err, user) {
          if (err) {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'This login already exists [' + err.message + ']'
              });
          }
          if (user){
            Item.findOne({ _id: updateId }, function(err, item){
                if (err) {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: err.message
                    });
                }
                if (item) {
                  if( user["_id"] == item["user_id"] ){
                      Item.update({ _id: updateId }, updateValues, {upsert:true}, function(err, doc){
                        if (err) {
                            dbController.closeDBConnection(db, res, 500, {
                                success: false,
                                response: false,
                                message: 'Update error. DB message: '+err.message
                            });
                        }
                        if (doc) {
                            item.user = user;
                            item.title  = updateValues['title'];
                            item.description = updateValues['description'];
                            dbController.closeDBConnection(db, res, 200, {
                                success: true,
                                response: item,
                                message: false
                            });
                        } else {
                            dbController.closeDBConnection(db, res, 500, {
                                success: false,
                                response: false,
                                message: 'Update error'
                            });
                        }
                      })

                  } else {
                    dbController.closeDBConnection(db, res, 403, {
                        success: false,
                        response: false,
                        message: 'This work does not belong to this user'
                    });
                  }
                } else {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: 'error get Item'
                    });
                }
            });
          } else {
              dbController.closeDBConnection(db, res, 500, {
                  success:    false,
                  response:   false,
                  message:    'Error in getting Users list'
              });
          }
      });
  });
};

module.exports.getMyItemsOrSearch = getMyItemsOrSearch;
module.exports.deleteMyItem       = deleteMyItem;
module.exports.getSingleItem      = getSingleItem;
module.exports.UpdateMyItems      = UpdateMyItems;
module.exports.CreateMyItems      = CreateMyItems;
