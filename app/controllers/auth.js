var path            = require('path');
var mongoose        = require('mongoose');
var crypto          = require('crypto');
var responseMessage = require(path.join(__dirname, '../../app/libs/responseMessage'));
var dbSchemas       = require(path.join(__dirname, '../../app/models'));
var dbController     = require(path.join(__dirname, '../../app/libs/dbController'));

function Other(req, res){
    responseMessage(res, 403, false, false, 'unknown API method');
}

function authorization(req, res){
    var requestBody = req.body;

    dbController.createDBConnection(false, res, function(db, res){
        var User = db.model("Users", dbSchemas.UserSchema);

        if (!requestBody.email || !requestBody.password) {

            dbController.closeDBConnection(db, res, 403, {
                success: false,
                response: false,
                message: 'login and password must be required'
            });

        } else {
            User.where({ email: requestBody.email }).findOne(function (err, user) {

                if (err) {
                    dbController.closeDBConnection(db, res, 500, {
                        success: false,
                        response: false,
                        message: 'DB error: ' + err.message
                    });
                }
                if (user) {
                    if (user.role != 'User') {
                        if (crypto.createHash('md5').update(requestBody.password).digest('hex') == user.password) {

                            var api_token = crypto.randomBytes(32).toString('hex');
                            User.findOneAndUpdate({ email: requestBody.email }, {api_token: api_token}, {upsert:true}, function(err, doc){
                                if (err) {

                                    dbController.closeDBConnection(db, res, 500, {
                                        success: false,
                                        response: false,
                                        message: 'DB error: ' + err.message
                                    });

                                }

                                if (doc) {

                                    dbController.closeDBConnection(db, res, 200, {
                                        success: true,
                                        response: {
                                            "api_token":        api_token
                                        },
                                        message: false
                                    });

                                } else {

                                    dbController.closeDBConnection(db, res, 500, {
                                        success: false,
                                        response: false,
                                        message: 'Error TOKEN generation'
                                    });

                                }
                            });

                        } else {

                            dbController.closeDBConnection(db, res, 403, {
                                success: false,
                                response: false,
                                message: 'Invalid login or password'
                            });

                        }
                    } else {
                        dbController.closeDBConnection(db, res, 403, {
                            success: false,
                            response: false,
                            message: 'Access denied'
                        });
                    }

                } else {

                    dbController.closeDBConnection(db, res, 403, {
                        success: false,
                        response: false,
                        message: 'Invalid login or password'
                    });

                }
            });
        }
    });
}

module.exports.other = Other;
module.exports.authorization = authorization;
