mainApp.config([
    '$urlRouterProvider',
    '$stateProvider',
    '$locationProvider',
    function($urlRouterProvider, $stateProvider, $locationProvider){
        $locationProvider.html5Mode({
            enabled:        true,
            requireBase:    false,
            rewriteLinks:   false
        });

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: '/templates/authorization.html',
                controller: 'authorizationCtrl'
            })
            .state('main', {
                url: '/main',
                templateUrl: '/templates/main.html',
                controller: 'mainCtrl'
            })
            .state('main.users', {
                url: '/users',
                templateUrl: '/templates/main/users.html',
                controller: 'usersCtrl'
            })
            .state('main.items', {
                url: '/items',
                templateUrl: '/templates/main/items.html',
                controller: 'itemsCtrl'
            })
            .state('main.UserSingle', {
                url: '/user/:id',
                templateUrl: '/templates/main/singleUser.html',
                controller: 'singleUserCtrl'
            })
            .state('main.itemSingle', {
                url: '/item/:id',
                templateUrl: '/templates/main/singleItem.html',
                controller: 'singleItemCtrl'
            });


        $urlRouterProvider.otherwise('/login');
    }
]);
