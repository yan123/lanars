mainApp.controller('searchUserDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    function($scope, $mdDialog, $translate){
        $translate([
            'searchUserDialog.title',
            'searchUserDialog.close',
            'searchUserDialog.name',
            'searchUserDialog.email',
            'searchUserDialog.done',
            'searchUserDialog.required',
        ]).then(function (data) {
            $scope.fieldValues = data;
        });

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            $mdDialog.hide($scope.searchUserDialog);
        };
    }
]);
