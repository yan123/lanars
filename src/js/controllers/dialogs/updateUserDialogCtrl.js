mainApp.controller('updateUserDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    'userOldData',
    function($scope, $mdDialog, $translate, userOldData){
        $scope.roles = [];
        $translate([
            'updateUserDialog.title',
            'updateUserDialog.close',
            'updateUserDialog.name',
            'updateUserDialog.phone',
            'updateUserDialog.email',
            'updateUserDialog.password',
            'updateUserDialog.done',
            'updateUserDialog.required',
            'updateUserDialog.minPassword',
            'updateUserDialog.maxPassword'
        ]).then(function (data) {
            $scope.fieldValues = data;

        });


        $scope.filterRoles = function(role){
            var currentRole = JSON.parse(window.localStorage.getItem('auth_info')).user.role;


            if (mask[currentRole].indexOf(role) != -1) {
                return true;
            } else {
                return false;
            }
        };

        $scope.email = userOldData.email;
        $scope.updateUser = {
            name:       userOldData.name,
            phone:       userOldData.phone
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            $mdDialog.hide($scope.updateUser);
        };
    }
]);
