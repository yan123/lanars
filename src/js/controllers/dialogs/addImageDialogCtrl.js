mainApp.controller('addImageDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    'Upload',
    'authControl',
    '$timeout',
    'id',
    function($scope, $mdDialog, $translate, Upload, authControl, $timeout, id){
        $translate([
            'updateUserDialog.title',
        ]).then(function (data) {
            $scope.fieldValues = data;
        });

        $scope.id = id;

        $scope.uploadFiles = function (files) {
          $scope.files = files;
          if (files && files.length) {
              Upload.upload({
                  url: '/api/item/'+id+'/image',
                  headers: {
                    Authorization : authControl.getToken()
                  },
                  contentType: 'multipart/form-data',
                  data: {
                      file: files
                  }
              }).then(function (response) {
                  $timeout(function () {
                      $scope.result = response.data;
                      $mdDialog.hide()
                  });
              }, function (response) {
                  if (response.status > 0) {
                      $scope.errorMsg = response.status + ': ' + response.data;
                  }
              }, function (evt) {
                  $scope.progress =
                      Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
              });
          }
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            // $mdDialog.hide($scope.updateUser);
        };
    }
]);
