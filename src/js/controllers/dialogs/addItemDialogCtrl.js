mainApp.controller('addItemDialogCtrl', [
    '$scope',
    '$mdDialog',
    '$translate',
    function($scope, $mdDialog, $translate){
        $scope.filterRoles = function(role){
            var currentRole = JSON.parse(window.localStorage.getItem('auth_info')).user.role;

            var mask = {
                Root: ['Root', 'Admin', 'User'],
                Admin: ['User']
            };

            if (mask[currentRole].indexOf(role) != -1) {
                return true;
            } else {
                return false;
            }
        };

        $scope.roles = [];
        $translate([
            'addItemDialog.titleH2',
            'addItemDialog.close',
            'addItemDialog.title',
            'addItemDialog.description',
            'addItemDialog.done',
            'addItemDialog.required',
        ]).then(function (data) {
            $scope.fieldValues = data;
        });



        $scope.addItem = {};

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.changeLng = function() {
            $mdDialog.hide();
        };

        $scope.sendForm = function(){
            $mdDialog.hide($scope.addItem);
        };
    }
]);
