mainApp.controller('singleUserCtrl', [
    '$scope',
    '$state',
    'Api',
    'authControl',
    'Toast',
    '$rootScope',
    '$translate',
    '$mdDialog',
    '$stateParams',
    function($scope, $state, Api, authControl, Toast, $rootScope, $translate, $mdDialog, $stateParams){

            getTranslates();
            updateItemList();
            var translateListener = $rootScope.$on('$translateChangeSuccess', getTranslates);
            $scope.$on('$destroy', translateListener);

              $scope.user = {
                  Data: [],
                  authControl_api : authControl.getToken()
              };

              function updateItemList(){
                Api.Users.single($stateParams.id,function(data){
                    $scope.user.Data = data.response;
                    $scope.loaded  = true;
                });
              }

              function getTranslates(){
                  $translate([
                      'Users.table.header.title',
                  ]).then(function (data) {
                      $scope.translations = data;
                  });
              }

          }
  ]);
