mainApp.controller('authorizationCtrl', [
    '$scope',
    'Api',
    'authControl',
    '$state',
    function($scope, Api, authControl, $state){
        if (authControl.getToken() != false) $state.go('main.users');

        $scope.auth = {};
        $scope.login = function(){
            Api.Auth.login({
                email:      $scope.auth.email,
                password:   $scope.auth.password
            });
        };
        $scope.register = function(){
            Api.Auth.register({
                name:      $scope.auth.name,
                email:      $scope.auth.email,
                password:   $scope.auth.password,
                phone:      $scope.auth.phone
            });
        };
    }
]);
