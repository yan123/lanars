mainApp.controller('mainCtrl', [
    '$scope',
    '$rootScope',
    'authControl',
    '$mdDialog',
    '$translate',
    '$state',
    'Api',
    'Toast',
    function($scope, $rootScope, authControl, $mdDialog, $translate, $state, Api, Toast){
        if (authControl.getToken() == false) $state.go('login');

        getTranslates();
        var translateListener = $rootScope.$on('$translateChangeSuccess', getTranslates);
        $scope.$on('$destroy', translateListener);

        $scope.navCurrent = $state.current.name;
        var stateChangeListener = $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            $scope.navCurrent = toState.name;
        });
        $scope.$on('$destroy', stateChangeListener);

        $scope.settingsDialog = function(event) {
            $mdDialog.show({
                controller: 'settingsDialogCtrl',
                templateUrl: 'templates/dialogs/settingsDialog.html',
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose:true,
                fullscreen: false
            });
        };

        $scope.nav = [
            {
                icon: '&#xE7EF;',
                name: 'main.users',
                action: 'users',
                divider: false,
                state: 'main.users'
            },
            {
                icon: '&#xE8F9;',
                name: 'main.items',
                action: 'items',
                divider: false,
                state: 'main.items'
            },
            {
                icon: '&#xE8B8;',
                name: 'main.settings',
                action: 'settings',
                divider: true,
                state: false
            },
            {
                icon: '&#xE879;',
                name: 'main.exit',
                action: 'logout',
                divider: false,
                state: false
            }

        ];
        console.log($scope.nav);
        $scope.navActions = {
            settings: $scope.settingsDialog,
            logout: function(){Api.Auth.logout()},
            users: function(){$state.go('main.users')},
            items: function(){$state.go('main.items')}
        };
        $scope.navStatus = false;

        function getTranslates(){
            $translate([
                'main.users',
                'main.items',
                'main.settings',
                'main.exit'
            ]).then(function (data) {
                $scope.translations = data;
            });
        }
    }
]);
